'use strict';

const BaseStrategy = require('./base');

class RoundRobinStrategy extends BaseStrategy
{
    constructor()
    {
        super();
        this.counter = 0;
    }

    select(list)
    {
        if (this.counter >= list.length) {
            this.counter = 0;
        }

        return list[this.counter++];
    }
}

module.exports = RoundRobinStrategy;
