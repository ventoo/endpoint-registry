'use strict';

const _ = require('lodash');

const StrategyMap = {
    Base: require('./base'),
    Random: require('./random'),
    RoundRobin: require('./round-robin')
};

function resolve(opt)
{
    let Strategy = StrategyMap.Random;
    if (StrategyMap.Base.isPrototypeOf(opt)) {
        Strategy = opt;
    } else if (_.isString(opt)) {
        const strategyType = Object.keys(StrategyMap).find(type => type.toLowerCase() === opt.toLowerCase());
        if (strategyType) {
            Strategy = StrategyMap[strategyType];
        } else {
            throw new Error(`Invalid strategy type '${opt}'`);
        }
    }

    return Strategy;
}

module.exports = { resolve, ...StrategyMap };
