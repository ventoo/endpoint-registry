'use strict';

const { random } = require('lodash');
const BaseStrategy = require('./base');

class RandomStrategy extends BaseStrategy
{
    select(list)
    {
        return list[random(0, list.length - 1)];
    }
}

module.exports = RandomStrategy;
