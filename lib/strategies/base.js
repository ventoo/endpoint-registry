'use strict';

class BaseStrategy
{
    select(/*list*/)
    {
        throw new Error('Not implemented method');
    }
}

module.exports = BaseStrategy;
