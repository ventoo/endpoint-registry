'use strict';

const _ = require('lodash');
const BaseEndpoints = require('./base');

class DummyEndpoints extends BaseEndpoints
{
    constructor(opt, Strategy)
    {
        super(opt, Strategy);
        this.opt = { urls: [], ...this.opt };
        this.opt.endpoints = _.isArray(this.opt.urls) ? this.opt.urls : [];
    }

    async loadEndpoints()
    {
        this.endpoints = this.opt.endpoints;
    }
}

module.exports = DummyEndpoints;
