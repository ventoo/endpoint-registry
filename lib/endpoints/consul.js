'use strict';

const _ = require('lodash');
const consul = require('consul')({ promisify: true });
const BaseEndpoints = require('./base');

function ensureIsValidService(service)
{
    if (!_.isString(service) || !service.length) {
        throw new Error(`Invalid service '${service}'`);
    }
}

function ensureIsValidTag(tag)
{
    if (!_.isString(tag) || !tag.length) {
        throw new Error(`Invalid tag '${tag}'`);
    }
}

class ConsulEndpoints extends BaseEndpoints
{
    constructor(opt, Strategy)
    {
        super(opt, Strategy);
        this.opt = { consulService: '', consulTag: '', ...this.opt };
        ensureIsValidService(this.opt.consulService);
        ensureIsValidTag(this.opt.consulTag);
    }

    async loadEndpoints()
    {
        const options = { service: this.opt.consulService, tag: this.opt.consulTag, passing: true };
        const nodes = await consul.health.service(options);
        this.endpoints = [];
        for (let node of nodes) {
            this.endpoints.push({ address: node.Node.Address, port: node.Service.Port });
        }
        consul.watch({ method: consul.health.service, options })
            .on('change', nodes => {
                this.endpoints = [];
                for (let node of nodes) {
                    this.endpoints.push({ address: node.Node.Address, port: node.Service.Port });
                }
                const endpoint = this.endpoints.find(node => _.isEqual(node, this.endpoint));
                if (!endpoint) {
                    this.emit('change', this.select(this.endpoints));
                }
            });
    }
}

module.exports = ConsulEndpoints;
