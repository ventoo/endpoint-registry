'use strict';

const _ = require('lodash');

const EndpointsMap = {
    Base: require('./base'),
    Dummy: require('./dummy'),
    Json: require('./json'),
    Consul: require('./consul')
};

function resolve(opt)
{
    let Endpoints = EndpointsMap.Dummy;
    if (EndpointsMap.Base.isPrototypeOf(opt)) {
        Endpoints = opt;
    } else if (_.isString(opt)) {
        const endpointsType = Object.keys(EndpointsMap).find(type => type.toLowerCase() === opt.toLowerCase());
        if (endpointsType) {
            Endpoints = EndpointsMap[endpointsType];
        } else {
            throw new Error(`Invalid endpoints type '${opt}'`);
        }
    }

    return Endpoints;
}

module.exports = { resolve, ...EndpointsMap };
