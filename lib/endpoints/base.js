'use strict';

const EventEmitter = require('events');
const _ = require('lodash');

class BaseEndpoints extends EventEmitter
{
    constructor(opt, Strategy)
    {
        super();
        this.strategy = new Strategy();
        this.opt = opt;
        this.endpoints = null;
        this.endpoint = null;
    }

    async loadEndpoints()
    {
        throw new Error('Not implemented method');
    }

    async next()
    {
        if (_.isNull(this.endpoints)) {
            await this.loadEndpoints();
        }

        return this.select(this.endpoints);
    }

    select(endpoints)
    {
        if (endpoints.length > 0) {
            if (1 === endpoints.length) {
                this.endpoint = endpoints[0];
            } else {
                this.endpoint = this.strategy.select(endpoints);
            }
        }

        return this.endpoint;
    }
}

module.exports = BaseEndpoints;
