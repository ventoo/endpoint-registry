'use strict';

const _ = require('lodash');
const fs = require('fs');
const { promisify } = require('util');
const readFile = promisify(fs.readFile);
const BaseEndpoints = require('./base');

function ensureIsValidPath(path)
{
    if (!_.isString(path) || !path.length) {
        throw new Error(`Invalid path '${path}'`);
    }
}

async function loadEndpoints(path)
{
    if (path.startsWith('file://')) {
        path = path.replace('file://', '');
    }
    let endpoints = [];
    const content = await readFile(path);
    if (content
        && content.length > 0
    ) {
        endpoints = JSON.parse(content);
        if (!_.isArray(endpoints)) {
            endpoints = [];
        }
    }

    return endpoints;
}

class JsonEndpoints extends BaseEndpoints
{
    constructor(opt, Strategy)
    {
        super(opt, Strategy);
        this.opt = { path: '', ...this.opt };
        ensureIsValidPath(this.opt.path);
    }

    async loadEndpoints()
    {
        this.endpoints = await loadEndpoints(this.opt.path);
    }
}

module.exports = JsonEndpoints;
