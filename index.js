'use strict';

const Endpoints = require('./lib/endpoints');
const Strategies = require('./lib/strategies');

function resolve(opt)
{
    return new (Endpoints.resolve(opt.endpointsType))(opt, Strategies.resolve(opt.endpointsStrategy));
}

module.exports = { resolve };
